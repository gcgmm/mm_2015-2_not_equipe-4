﻿O jogo está na pasta "PolinomiAr".
Para o jogo funcionar corretamente, é preciso a pasta "PolinomiAr" tenha os arquivos Comparador.jar e Banco.txt.

Comparador.jar: É quem faz a validação dos polinômios. O código fonte dele está na pasta "ValidadorPolinomios".
Banco.txt: É de onde vem as questões do jogo. As instruções de como utililizar estão dentro do próprio arquivo