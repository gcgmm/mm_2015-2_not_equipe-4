﻿using Assets.Validacao.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Assets.Validacao
{
    public class Jogo
    {
        private const string COMPARADOR = "Comparador.jar";
        private const string BANCO = "Banco.txt";
        private const int QUESTOES_POR_PARTIDA = 3;
        private const int PONTUACAO_MAX = 100;
        private const int PERDA_POR_ERRO = 10;
        private const int PONTUACAO_MIN = 10;

        public string NomeJogador { get; set; }
        public Modalidade ModalidadeAtual { get; private set; }
        public Questao QuestaoAtual { get; private set; }
        public int PontuacaoTotal { get; private set; }
        public int PontuacaoQuestao { get; private set; }
        public int QuestoesJaResolvidas { get; private set; }
        public int QuantidadeErrosQuestao { get; internal set; }
        public int QuantidadeTotolErros { get; internal set; }
        public bool EstaEmJogo { get { return ModalidadeAtual != null; } }

        private List<Questao> jaUsadas = new List<Questao>();
        private List<Modalidade> modalidades;
        private Modalidade modalidadeTodos = new Modalidade("Todos");
        private Modalidade modalidadeModoLivre = new Modalidade("Modo livre");
        private Validador validador;
        private static Jogo instance;

        private Jogo()
        {
            try
            {
                if (!File.Exists(COMPARADOR))
                    throw new Exception("Arquivo " + COMPARADOR + " não encontrato.");
                if (!File.Exists(BANCO))
                    throw new Exception("Arquivo " + BANCO + " não encontrato.");

                validador = Validador.Create(COMPARADOR);
                modalidades = new List<Modalidade>();
                CarregarQuestoes(BANCO);
                modalidades.Add(modalidadeTodos);
                modalidades.Add(modalidadeModoLivre);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao inicializar o jogo.", ex);
            }
        }

        public static Jogo Instance
        {
            get
            {
                if (instance == null)
                    instance = new Jogo();
                return instance;
            }
        }

        public static bool HasCreated
        {
            get
            {
                return instance != null;
            }
        }

        public bool EhModalidadeTodos
        {
			get { return modalidadeTodos.Equals(ModalidadeAtual); }
        }

        public bool EhModalidadeLivre
        {
			get { return modalidadeModoLivre.Equals(ModalidadeAtual); }
        }

        /// <summary>
        /// Retura True se o jogo já terminou
        /// </summary>
        /// <returns></returns>
        public bool Terminou()
        {
            return QuestoesJaResolvidas >= QUESTOES_POR_PARTIDA;
        }

        public Modalidade SelecionarModalidade(int index)
        {
            Resetar();
            ModalidadeAtual = modalidades[index];
            return ModalidadeAtual;
        }

        public Questao SortearQuestao()
        {
            try
            {
                var questoesModalidade = new List<Questao>();
                if (ModalidadeAtual.Equals(modalidadeModoLivre) || ModalidadeAtual.Equals(modalidadeTodos))
                {
                    modalidades.ForEach(modalidade => questoesModalidade.AddRange(modalidade.Questoes));
                }
                else
                {
                    questoesModalidade = ModalidadeAtual.Questoes;
                }

                if (!TemQuestaoLivre(questoesModalidade))
                    throw new Exception("Todas as questão dessa modalidade já foram utilizadas");

                QuestaoAtual = null;
                do
                {
                    //questao = questoesModalidade[new UnityEngine.Random().Next(0, questoesModalidade.Count() - 1)];
                    int rand = UnityEngine.Random.Range(0, questoesModalidade.Count() - 1);
                    QuestaoAtual = questoesModalidade[rand];
                } while (jaUsadas.Contains(QuestaoAtual));

                jaUsadas.Add(QuestaoAtual);

                PontuacaoQuestao = PONTUACAO_MAX;
                QuantidadeErrosQuestao = 0;

                return QuestaoAtual;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao sortear a questão", ex);
            }
        }

        private bool TemQuestaoLivre(List<Questao> questoesModalidade)
        {
            foreach (var item in questoesModalidade)
            {
                if (!jaUsadas.Contains(item))
                {
                    return true;
                }
            }

            return false;
        }

        public bool Validar(string resposta, out string mensagem)
        {
            try
            {
                bool resultado = false;
                mensagem = "";
                var result = validador.Validar(resposta);
                if (result != ResultadosJava.Correto)
                {
                    mensagem = "Polinômio inválido";
                    Debug.Log("Polinomio inválido. Retorno: " + result.ToString());
                }
                else
                {
                    if (EhModalidadeLivre)
                    {
                        if (EhIgual(resposta, QuestaoAtual.Polinomio))
                        {
                            mensagem = "Incorreto!\r\nA resposta está igual ou muido semelhante";
                        }
                        else if (!EhEquivalente(resposta, QuestaoAtual.Polinomio))
                        {
                            mensagem = "Incorreto!\r\nA resposta não é equivalente à questão";
                        }
                        else
                        {
                            mensagem = "Correto!\r\nA resposta é equivalente ao polinômio da questão";
                            resultado = true;
                        }
                    }
                    else
                    {
                        if (EhIgual(resposta, QuestaoAtual.Respostas))
                        {
                            mensagem = "Correto!\r\nA resposta é igual ou muito semelhante à alguma das repostas possíveis";
                            resultado = true;
                        }
                        else
                        {
                            mensagem = "Incorreto!\nA resposta está diferente de todas as repostas possíveis";
                        }
                    }
                }

                if (resultado)
                {
                    PontuacaoTotal += PontuacaoQuestao;
                    QuestoesJaResolvidas++;
                }
                else
                {
                    QuantidadeTotolErros++;
                    QuantidadeErrosQuestao++;
                    PontuacaoQuestao -= PERDA_POR_ERRO;
                    if (PontuacaoQuestao < PONTUACAO_MIN)
                        PontuacaoQuestao = PONTUACAO_MIN;
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao validar a resposta.", ex);
            }
        }

        public void Resetar()
        {
            ModalidadeAtual = null;
            PontuacaoTotal = 0;
            PontuacaoQuestao = 0;
            QuestoesJaResolvidas = 0;
            QuantidadeErrosQuestao = 0;
            QuantidadeTotolErros = 0;
            jaUsadas.Clear();
        }

        public string GetNomeModalidadeQuestao()
        {
            string modalidade;
            if (!Jogo.Instance.EhModalidadeLivre)
                modalidade = QuestaoAtual.modalidade.Descricao;
            else
                modalidade = Jogo.Instance.ModalidadeAtual.Descricao;
            return modalidade;
        }

        private bool EhIgual(string poli, List<string> respostasPossiveis)
        {
            foreach (var respostaPossivel in respostasPossiveis)
            {
                if (EhIgual(poli, respostaPossivel))
                {
                    return true;
                }
            }
            return false;
        }

        private bool EhIgual(string poli1, string poli2)
        {
            var result = validador.ValidarIgualdade(poli1, poli2);
            if (result == ResultadosJava.Correto)
            {
                return true;
            }
            else if (result != ResultadosJava.Incorreto)
            {
                throw new Exception("Ocorreu algum erro: " + result);
            }
            return false;
        }

        private bool EhEquivalente(string poli1, string poli2)
        {
            var result = validador.ValidarEquivalencia(poli1, poli2);
            if (result == ResultadosJava.Correto)
            {
                return true;
            }
            else if (result != ResultadosJava.Incorreto)
            {
                throw new Exception("Ocorreu algum erro: " + result);
            }
            return false;
        }

        private void CarregarQuestoes(string localExpressoes)
        {
            using (StreamReader sr = new StreamReader(localExpressoes))
            {
                const string modalidade = @"^%(.+)";
                const string comentario = @"//.+";
                Modalidade modalidadeAux = null;

                string linha = "";
                int contador = 0;
                while ((linha = sr.ReadLine()) != null)
                {
                    try
                    {
                        contador++;

                        linha = Regex.Replace(linha, comentario, "");
                        if (string.IsNullOrEmpty(linha.Trim()))
                            continue;

                        var match = Regex.Match(linha, modalidade);

                        if (match.Success)
                        {
                            modalidadeAux = new Modalidade(match.Groups[1].Value);
                            this.modalidades.Add(modalidadeAux);
                        }
                        else
                        {
                            var partesLinha = linha.Split(',');
                            if (partesLinha.Length < 2)
                                throw new Exception("A linha não possui os dados necessários");
                            Questao questao = new Questao(modalidadeAux, partesLinha[0]);

                            for (int i = 1; i < partesLinha.Length; i++)
                                questao.AddResposta(partesLinha[i]);

                            modalidadeAux.Questoes.Add(questao);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Erro na leitura do Banco.txt, na linha {0}: {1}", contador, linha), ex);
                    }
                }
            }
        }

    }
}
