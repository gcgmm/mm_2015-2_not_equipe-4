﻿using Assets.Validacao.Model;
using Assets.Validacao.Validacao;
using System;
using System.Diagnostics;

namespace Assets.Validacao
{
    public class Validador
    {
        private string caminhoComparador;
        private static Validador instance;

        private Validador(string caminhoComparador)
        {
            this.caminhoComparador = caminhoComparador;
        }

        public static Validador Create(string caminho)
        {
            if (instance != null)
                throw new Exception("O validador já foi criado!");
            return instance = new Validador(caminho);
        }

        public static Validador Instance
        {
            get
            {
                if (instance == null)
                    throw new Exception("O validador não foi criado!");
                return instance;
            }
        }

        private string Padronizar(string polinomio)
        {
            if (polinomio.Contains("\\"))
                throw new DivisaoNaoSuportadaException("Divisão não é suportada");
            return polinomio.Replace(".", "*").Replace(" ", "");
        }

        public ResultadosJava Validar(string poli)
        {
            try
            {
                var parametros = OperacaoJava.VerificaSeValido + " " + Padronizar(poli);
                int exitCode = IniciarProcesso(parametros);
                return (ResultadosJava)exitCode;
            }
            catch (DivisaoNaoSuportadaException)
            {
                return ResultadosJava.ErroLexico;
            }
        }

        public ResultadosJava ValidarIgualdade(string poli1, string poli2)
        {
            var parametros = OperacaoJava.VerificaIgualdade + " ";
            parametros += Padronizar(poli1) + " " + Padronizar(poli2);
            int exitCode = IniciarProcesso(parametros);
            return (ResultadosJava)exitCode;
        }

        public ResultadosJava ValidarEquivalencia(string poli1, string poli2)
        {
            var parametros = OperacaoJava.VerificaEquivalencia + " ";
            parametros += Padronizar(poli1) + " " + Padronizar(poli2);
            int exitCode = IniciarProcesso(parametros);
            return (ResultadosJava)exitCode;
        }

        private int IniciarProcesso(string parametros)
        {
            try
            {
                var processInfo = new ProcessStartInfo(caminhoComparador, parametros);

                Process proc;

                if ((proc = Process.Start(processInfo)) == null)
                {
                    throw new Exception("Não foi possível executar o Comparador.jar");
                }

                proc.WaitForExit();
                int exitCode = proc.ExitCode;
                proc.Close();
                return exitCode;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível executar o Comparador.jar, verifique se ele está no mesmo local que o executável.", ex);
            }
        }
    }
}
