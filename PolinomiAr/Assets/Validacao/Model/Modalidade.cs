﻿using System.Collections.Generic;

namespace Assets.Validacao.Model
{
    public class Modalidade
    {
        public string Descricao { get; private set; }
        public List<Questao> Questoes { get; private set; }

        public Modalidade(string Descricao)
        {
            this.Questoes = new List<Questao>();
            this.Descricao = Descricao;
        }

        public override bool Equals(object obj)
        {
            var outraMod = obj as Modalidade;
            if(obj == null)
                return false;
            return outraMod.Descricao.Equals(Descricao);
        }

        public override int GetHashCode()
        {
            return Descricao.GetHashCode();
        }
    }
}
