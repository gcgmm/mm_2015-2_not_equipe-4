﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Validacao.Model
{
    public enum ResultadosJava
    {
        Correto = 1,
        Incorreto = 2,
        ParametrosIncorretos = 100,
        ErroLexico = 101,
        ErroSintatico = 102,
        ErroSemantico = 103,
        OperacaoInvalida = 104,
        NaoDeviaTerChegoAqui = 105,
        NaoImplementado = 106,
        ErroOutro = 199
    }
}
