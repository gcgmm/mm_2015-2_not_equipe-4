﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Validacao.Model
{
    class DivisaoNaoSuportadaException : Exception
    {
        public DivisaoNaoSuportadaException(string message) : base(message)
        {
        }
    }
}
