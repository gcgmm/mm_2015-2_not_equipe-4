﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Validacao.Model
{
    public class Questao
    {
        public Modalidade modalidade { get; private set; }
        /// <summary>
        /// Polinômio da questão que deverá ser resolvido
        /// </summary>
        public string Polinomio { get; private set; }
        /// <summary>
        /// Respostas possíveis para a questão
        /// </summary>
        public List<string> Respostas { get; set; }

        public Questao(Modalidade modalidade, string questao, params string[] respostas)
        {
            this.Respostas = new List<string>();
            this.modalidade = modalidade;
            this.Polinomio = questao;
            foreach (var resposta in respostas)
            {
                AddResposta(resposta);
            }
        }

        public void AddResposta(string resposta)
        {
            ResultadosJava resultado;
            if ((resultado = Validador.Instance.ValidarEquivalencia(Polinomio, resposta)) !=
                ResultadosJava.Correto)
            {
                StringBuilder str = new StringBuilder();
                str.Append("Erro ao preencher o banco de questões.")
                    .Append("\r\nResultado para validação de equivalência: ")
                    .Append(resultado.ToString()).Append(". ")
                    .Append("\r\nQuestao: \"").Append(Polinomio)
                    .Append("\"\r\nResposta: \"").Append(resposta).Append("\"");

                throw new Exception(str.ToString());
            }

            Respostas.Add(resposta);
        }
    }
}
