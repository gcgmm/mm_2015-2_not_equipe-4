﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Validacao.Validacao
{
    public enum OperacaoJava
    {
        VerificaSeValido = 1,
        VerificaEquivalencia = 2,
        VerificaIgualdade = 3,
        VerificaSomaSubtracao = 4, //não implementado
        VerificaFatoracao = 5, //não implementado
        VerificaPropriedadeDistribuida = 6, //não implementado
        VerificaProdutosNotaveis = 7, //não implementado
    }
}
