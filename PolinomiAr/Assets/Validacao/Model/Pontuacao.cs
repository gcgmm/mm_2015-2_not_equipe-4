﻿
using System;

namespace Assets.Validacao.Model
{
    [Serializable]
    public class Pontuacao
    {
        public string Jogador;
        public int Pontos;
        public int TotalErros;
    }
}
