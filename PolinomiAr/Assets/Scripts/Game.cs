﻿using Assets.Scripts.Menu;
using Assets.Validacao;
using Assets.Validacao.Model;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class Game : MonoBehaviour
    {
        public static string mensagemValidacao;
        public static string respostaDada;
        Text txtSaida;
        InputField txtResposta;

        public Game()
        {

        }

        public void Start()
        {
            txtResposta = GameObject.Find("text-resposta").GetComponent<InputField>();
            txtSaida = GameObject.Find("saida").GetComponent<Text>();

            GameObject.Find("text-nome-jogador").GetComponent<Text>().text = 
                Jogo.Instance.NomeJogador;

            txtResposta.text = "";
            txtSaida.text = "";

            TrocarQuestao();
        }

        public void Validar()
        {
            try
            {
                if (Jogo.Instance.Validar(txtResposta.text, out mensagemValidacao))
                {
                    respostaDada = txtResposta.text;
                    Application.LoadLevel("ScoreQuestion");
                }
                else
                {
                    txtSaida.text = mensagemValidacao;
                    AtualizarPontuacao();
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
                txtSaida.text = ex.Message;
            }

            AtualizarPontuacao();
        }

        public void Voltar()
        {
            Application.LoadLevel("Modality");
        }

        private void TrocarQuestao()
        {
            Jogo.Instance.SortearQuestao();

            txtResposta.text = "";
            GameObject.Find("modalidade").GetComponent<Text>().text = Jogo.Instance.GetNomeModalidadeQuestao();
            GameObject.Find("questao").GetComponent<Text>().text = Jogo.Instance.QuestaoAtual.Polinomio;
            AtualizarPontuacao();
        }

        private void AtualizarPontuacao()
        {
            GameObject.Find("text-pontuacao-questao").GetComponent<Text>().text = "Questão: " + Jogo.Instance.PontuacaoQuestao;
            GameObject.Find("text-pontuacao-total").GetComponent<Text>().text = "Total: " + Jogo.Instance.PontuacaoTotal;
        }
    }
}
