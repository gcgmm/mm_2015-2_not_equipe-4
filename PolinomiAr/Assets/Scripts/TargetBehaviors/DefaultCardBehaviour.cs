﻿using UnityEngine;
using System.Collections;
using Vuforia;

namespace Assets.Scripts {

	class DefaultCardBehaviour : MonoBehaviour, ITrackableEventHandler {

		private TrackableBehaviour mTrackableBehaviour;

		public GameVuforia cardSceneController;
		public string recognitionExpression;

			
		// Use this for initialization
		void Start() {
			mTrackableBehaviour = GetComponent<TrackableBehaviour>();
			if (mTrackableBehaviour)
			{
				mTrackableBehaviour.RegisterTrackableEventHandler(this);
			}
		}
		
		
		/// <summary>
		/// Implementation of the ITrackableEventHandler function called when the
		/// tracking state changes.
		/// </summary>
		public void OnTrackableStateChanged(
			TrackableBehaviour.Status previousStatus,
			TrackableBehaviour.Status newStatus) {
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
			    newStatus == TrackableBehaviour.Status.TRACKED ||
			    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
				OnTrackingFound ();
			} else {
				OnTrackingLost ();
			}
		}
		
		virtual public void OnTrackingFound() {
			Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider> (true);
			
			// Enable rendering:
			foreach (Renderer component in rendererComponents) {
				component.enabled = true;
			}
			
			// Enable colliders:
			foreach (Collider component in colliderComponents) {
				component.enabled = true;
			}

			if (recognitionExpression != null) {
				cardSceneController.AnexarSimboloResposta (recognitionExpression);
			}
		}
		
		
		virtual public void OnTrackingLost() {
			Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider> (true);
			
			// Disable rendering:
			foreach (Renderer component in rendererComponents) {
				component.enabled = false;
			}
			
			// Disable colliders:
			foreach (Collider component in colliderComponents) {
				component.enabled = false;
			}
		}
		
	}

}
