namespace Assets.Scripts
{
    class LimparUltimoCardBehaviour : DefaultCardBehaviour
    {
        public override void OnTrackingFound()
        {
            base.OnTrackingFound();
            cardSceneController.LimparUltimo();
        }
    }
}