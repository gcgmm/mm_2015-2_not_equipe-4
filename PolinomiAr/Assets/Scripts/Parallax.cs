﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public GameObject front;
	public GameObject back;
	public float variation = 0.05f;
	private int[] screenCenter;
	private Vector3 originalFront;
	private Vector3 originalBack;

	void Start() {
		screenCenter = new int[] { Screen.width / 2, Screen.height / 2 };
		if (back) {
			originalBack = back.transform.position;
		}
		if (front) {
			originalFront = front.transform.position;
		}

	}

	// Update is called once per frame
	void Update () {
		float newX = (Input.mousePosition.x - screenCenter[0]) * -variation;
		float newY = (Input.mousePosition.y - screenCenter[1]) * -variation;

		if (originalFront != null && front != null) {
			front.transform.position = new Vector3(newX + originalFront.x, newY + originalFront.y, front.transform.position.z);
		}
		if (originalBack != null && back != null) {
			back.transform.position = new Vector3(-newX + originalBack.x, -newY + originalBack.y, back.transform.position.z);
		}
		
	
	}
}
