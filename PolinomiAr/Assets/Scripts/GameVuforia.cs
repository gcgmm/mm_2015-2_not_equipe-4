﻿using Assets.Validacao;
using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class GameVuforia : MonoBehaviour
    {
        public static string mensagemValidacao;
        public static string respostaDada;

        Text txtAlertas;
        Text txtQuestao;
        Text txtResposta;
        Text txtModalidade;
        Text txtNomeJogador;

        public GameVuforia()
        {
            mensagemValidacao = string.Empty;
            respostaDada = string.Empty;
        }

        public void Start()
        {
            txtModalidade = GameObject.Find("text-modalidade").GetComponent<Text>();
            txtQuestao = GameObject.Find("text-questao").GetComponent<Text>();
            txtResposta = GameObject.Find("text-polinomio-entrada").GetComponent<Text>();
            txtAlertas = GameObject.Find("text-alerta").GetComponent<Text>();
            txtNomeJogador = GameObject.Find("text-nome-jogador").GetComponent<Text>();

            txtNomeJogador.text = Jogo.Instance.NomeJogador;
            txtResposta.text = "";
            txtAlertas.text = "";

            TrocarQuestao();
        }

        public void Validar()
        {
            try
            {
                Debug.Log("Validar o polinomio: " + respostaDada);
                if (Jogo.Instance.Validar(respostaDada, out mensagemValidacao))
                {
                    Application.LoadLevelAsync("ScoreQuestion");
                }
                else
                {
                    txtAlertas.text = mensagemValidacao;
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
                txtAlertas.text = ex.Message;
            }
        }

        public void Voltar()
        {
            Application.LoadLevelAsync("Modality");
        }

        private void TrocarQuestao()
        {
            Jogo.Instance.SortearQuestao();

            txtResposta.text = "";
            txtModalidade.text = Jogo.Instance.GetNomeModalidadeQuestao();
            txtQuestao.text = FormatarPolinomio(Jogo.Instance.QuestaoAtual.Polinomio);
        }

        internal void AnexarSimboloResposta(string part)
        {
            respostaDada += part;
            ExibiResposta();
        }

        internal void LimparResposta()
        {
            respostaDada = "";
            ExibiResposta();
        }

        internal void LimparUltimo()
        {
            if (respostaDada.Length > 0)
            {
                respostaDada = Regex.Replace(respostaDada, @"(\^)?.$", "");
                ExibiResposta();
            }
        }

        private void ExibiResposta()
        {
            txtResposta.text = "Resposta: " + FormatarPolinomio(respostaDada);
        }

        private static string FormatarPolinomio(string polinomio)
        {
            return polinomio
                .Replace("^1", "¹")
                .Replace("^2", "²")
                .Replace("^3", "³")
                .Replace("*", ".");
        }
    }
}
