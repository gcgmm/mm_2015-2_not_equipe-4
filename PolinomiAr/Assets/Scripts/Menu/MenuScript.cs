﻿using UnityEngine;
using System.Collections;
using Assets.Validacao;

public class MenuScript : MonoBehaviour
{
    public MenuScript()
    {
        if (Jogo.HasCreated)
        {
            Jogo.Instance.Resetar();
        }
    }
    public void onButtonStartClick()
    {
        Application.LoadLevel("Modality");
    }

    public void onBtnScoreClick()
    {
        Application.LoadLevel("Score");
    }
}
