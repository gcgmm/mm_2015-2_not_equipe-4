﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Validacao;

namespace Assets.Scripts.Menu
{
    class ModalityMenu : MonoBehaviour
    {
        private int modalidadeSelecionada = 0;
        private GameObject btnPlay;
        private GameObject btnPlayVuforia;
        private GameObject canvasNomeJogador;
        private Text txtExemplo1;
        private Text txtExemplo2;
        public static bool UsarVuforia;

        public ModalityMenu()
        {
            if (Jogo.HasCreated)
            {
                Jogo.Instance.Resetar();
            }
        }

        public void Start()
        {
            btnPlay = GameObject.Find("button-play");
            btnPlayVuforia = GameObject.Find("button-play-vuforia");
            canvasNomeJogador = GameObject.Find("canvas-nome-jogador");
            txtExemplo1 = GameObject.Find("text-example-row1").GetComponent<Text>();
            txtExemplo2 = GameObject.Find("text-example-row2").GetComponent<Text>();

            txtExemplo1.text = "";
            txtExemplo2.text = "";
            btnPlay.SetActive(false);
            btnPlayVuforia.SetActive(false);
            canvasNomeJogador.SetActive(false);
        }

        public void onButtonBackClick()
        {
            Application.LoadLevel("Menu");
        }

        public void onButtonBackUserNameClick()
        {
            canvasNomeJogador.SetActive(false);
        }

        public void onBtnPlayClick()
        {
            UsarVuforia = false;
            canvasNomeJogador.SetActive(true);
        }

        public void onBtnPlayVuforiaClick()
        {
            UsarVuforia = true;
            canvasNomeJogador.SetActive(true);
        }

        public void onBtnOkClick()
        {
            Jogo.Instance.NomeJogador =
                GameObject.Find("input-nome-jogador").GetComponent<InputField>().text;
            Jogo.Instance.Resetar();
            Jogo.Instance.SelecionarModalidade(modalidadeSelecionada);
            if (!UsarVuforia)
                Application.LoadLevel("Game");
            else
                Application.LoadLevel("GameVuforia");
        }

        public void onBtnSumClick()
        {
            modalidadeSelecionada = 0;
            this.changeExample("x + x", "2x");
        }

        public void onBtnNatebleProductClick()
        {
            modalidadeSelecionada = 1;
            this.changeExample("(a + b) . (a + b)", "a² + 2ab + b²");
        }

        public void onBtnDistributedClick()
        {
            modalidadeSelecionada = 2;
            this.changeExample("2 . (3x + 1)", "6x + 2");
        }

        public void onBtnFatorateClick()
        {
            modalidadeSelecionada = 3;
            this.changeExample("x² + 10x + 25", "(x + 5) . (x + 5)");
        }

        public void onBtnAllClick()
        {
            modalidadeSelecionada = 4;
            this.changeExample("????", "????");
        }

        public void onBtnFreeModeClick()
        {
            modalidadeSelecionada = 5;
            this.changeExample("????", "????");
        }

        private void changeExample(string exampleRow1, string exampleRow2)
        {
            btnPlayVuforia.SetActive(true);
            txtExemplo1.text = exampleRow1;
            txtExemplo2.text = exampleRow2;
        }
    }
}
