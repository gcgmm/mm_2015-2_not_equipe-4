﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Validacao;
using Assets.Validacao.Model;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System;

public class Score : MonoBehaviour
{
    private const string ARQUIVO = "Score";
    private Text txtPontuacaoTotal;
    private Text txtScores;

    void Start()
    {
        txtPontuacaoTotal = GameObject.Find("text-pontuacao-total").GetComponent<Text>();
        txtScores = GameObject.Find("text-scores").GetComponent<Text>();

        txtPontuacaoTotal.text = "";
        txtScores.text = "";

        var scores = RecuperarScore();

        if (Jogo.HasCreated && Jogo.Instance.EstaEmJogo)
        {
            txtPontuacaoTotal.text = string.Format("Total: {0} pts", Jogo.Instance.PontuacaoTotal);
            scores.Add(new Pontuacao
            {
                Jogador = Jogo.Instance.NomeJogador,
                Pontos = Jogo.Instance.PontuacaoTotal,
                TotalErros = Jogo.Instance.QuantidadeTotolErros
            });
            GravarScore(scores);
        }

        scores = scores.OrderByDescending(a => a.Pontos).ThenBy(a => a.TotalErros).ToList();

        string scoresText = "";
        foreach (var item in scores)
        {
            scoresText += string.Format("{0}: {1} pts, {2} erros\r\n",
                item.Jogador,
                item.Pontos,
                item.TotalErros);
        }

        if (scoresText.Length > 0)
            scoresText = scoresText.Substring(0, scoresText.Length - 2);
        else
            scoresText = "Nenhuma pontuação registrada";

        txtScores.text = scoresText;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Voltar()
    {
        Application.LoadLevel("Menu");
    }

    private List<Pontuacao> RecuperarScore()
    {
        try
        {
            if (!File.Exists(ARQUIVO))
                return new List<Pontuacao>();
            using (var fileStream = new FileStream(ARQUIVO, FileMode.Open, FileAccess.Read))
            {
                MemoryStream stream = new MemoryStream();
                IFormatter formatter = new BinaryFormatter();
                var scores = (List<Pontuacao>)formatter.Deserialize(fileStream);
                return scores;
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
            return new List<Pontuacao>();
        }
    }

    private void GravarScore(List<Pontuacao> scores)
    {
        using (var fileStream = new FileStream(ARQUIVO, FileMode.Create, FileAccess.Write))
        {
            var scoresSerializado = SerializeToStream(scores);
            scoresSerializado.WriteTo(fileStream);
        }
    }

    private static MemoryStream SerializeToStream(object o)
    {
        MemoryStream stream = new MemoryStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, o);
        return stream;
    }

    private static object DeserializeFromStream(MemoryStream stream)
    {
        IFormatter formatter = new BinaryFormatter();
        stream.Seek(0, SeekOrigin.Begin);
        object o = formatter.Deserialize(stream);
        return o;
    }
}
