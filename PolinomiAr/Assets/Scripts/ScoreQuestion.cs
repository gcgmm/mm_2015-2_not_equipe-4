﻿using Assets.Scripts.Menu;
using Assets.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ScoreQuestion : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {
            GameObject.Find("text-pontuacao-questao").GetComponent<Text>().text = 
                "Pontuação feita: " + Jogo.Instance.PontuacaoQuestao;
            GameObject.Find("text-numero-erros").GetComponent<Text>().text = 
                "Número de erros: " + Jogo.Instance.QuantidadeErrosQuestao;

            var txtSaida = GameObject.Find("text-mensagem").GetComponent<Text>();
            txtSaida.text = "Modalidade: " + Jogo.Instance.GetNomeModalidadeQuestao();
            txtSaida.text += "\r\nQuestão: " + Jogo.Instance.QuestaoAtual.Polinomio;
            if (!ModalityMenu.UsarVuforia)
            {
                txtSaida.text += "\r\nRepostada dada: " + Game.respostaDada;
                txtSaida.text += "\r\n" + Game.mensagemValidacao;
            }
            else
            {
                txtSaida.text += "\r\nRepostada dada: " + GameVuforia.respostaDada;
                txtSaida.text += "\r\n" + GameVuforia.mensagemValidacao;
            }
        }

		public void ProximaQuestaoWrapper()
		{
			ProximaQuestao ();
		}

        public static void ProximaQuestao()
        {
            if (Jogo.Instance.Terminou())
            {
                Application.LoadLevel("Score");
            }
            else
            {
                if (!ModalityMenu.UsarVuforia)
                {
                    Application.LoadLevel("Game");
                }
                else
                {
                    Application.LoadLevel("GameVuforia");
                }
            }
        }
    }
}
